<?php

defined('MW_PATH') || exit('No direct script access allowed');

/**
 * UI/UX Improvement 
 *
 * Initial List Create when customer register from customer area for mailwizz
 *
 * @package MailWizz EMA
 * @subpackage  Initial List Create extension when customer register from customer area for mailwizz
 * @author Masih Fathi <masihfathi@gmail.com>
 * @link https://www.avangemail.com/
 * @license https://www.avangemail.com/license/
 */
class UiUxImprovmentExt extends ExtensionInit {

    // name of the extension as shown in the backend panel
    public $name = 'UI/UX Improvement';
    // description of the extension as shown in backend panel
    public $description = 'Initial List Create when customer register from customer area for mailwizz';
    // current version of this extension
    public $version = '1.0';
    // the author name
    public $author = 'Masih Fathi';
    // author website
    public $website = 'https://www.avangemail.com/';
    // contact email address
    public $email = 'masihfathi@gmail.com';
    // in which apps this extension is allowed to run
    public $allowedApps = array('customer');
    // can this extension be deleted? this only applies to core extensions.
    protected $_canBeDeleted = true;
    // can this extension be disabled? this only applies to core extensions.
    protected $_canBeDisabled = true;
    // the extension model
    protected $_extModel;

    public function run() {
        if ($this->isAppName('customer')) {
            // handle all customer related tasks
            $this->customerApp();
        }
    }

    protected function customerApp() {
        $hooks = Yii::app()->hooks;
        $companyRequired = Yii::app()->options->get('system.customer_registration.company_required', 'no') == 'yes';
        if ($companyRequired) {
            $hooks->addAction('customer_model_customercompany_aftersave', function($company) {
                if (Yii::app()->getController()->getRoute() != 'guest/register') {
                    return;
                }
                $this->createInitialList($company->customer, $company);
            });
        } else {
            $hooks->addAction('customer_model_customer_aftersave', function($customer) {
                if (Yii::app()->getController()->getRoute() != 'guest/register') {
                    return;
                }
                $this->createInitialList($customer, null);
            });
        }
    }

    protected function createInitialList($customer, $company = null) {
        $request = Yii::app()->request;
        $list = new Lists();
        $listDefault = new ListDefault();
        $listCompany = new ListCompany();
        $listCustomerNotification = new ListCustomerNotification();
        $list->customer_id = (int) $customer->customer_id;
        // to create the default mail list fields.
        $list->attachBehavior('listDefaultFields', array(
            'class' => 'customer.components.db.behaviors.ListDefaultFieldsBehavior',
        ));
        $listDefault->mergeWithCustomerInfo($customer);
        $list->opt_in = Lists::OPT_IN_SINGLE;
        $list->opt_out = Lists::OPT_IN_SINGLE;
        if(!is_null($company) && !empty($company->name)){
            $list->name = $company->name;
            $list->display_name = $company->name;
            $list->description = $company->name . ' description';
        }else {
            $list->name = 'Initial Created List-' . $customer->last_name;
            $list->display_name = 'Initial Created List-' . $customer->last_name;
            $list->description = 'Initial Created List Description';
        }
        $list->visibility = Lists::VISIBILITY_PUBLIC;
        $list->merged = Lists::TEXT_NO;
        $list->welcome_email = Lists::TEXT_NO;
        $list->removable = Lists::TEXT_YES;
        $list->subscriber_require_approval = Lists::TEXT_NO;
        // create empty list
        if ($list->save()) { 
            $listCustomerNotification->list_id = $list->list_id;
            $listCustomerNotification->daily = ListCustomerNotification::TEXT_NO;
            $listCustomerNotification->subscribe = ListCustomerNotification::TEXT_NO;
            $listCustomerNotification->unsubscribe = ListCustomerNotification::TEXT_NO;
            // create notification settings for the list
            $listCustomerNotification->save();
            $listCompany->list_id = $list->list_id;
            if (!is_null($company) && !empty($company)) {
                $listCompany->mergeWithCustomerCompany($company);
            } else {
                // default set to france
                $listCompany->country_id = 73;
                $listCompany->name = 'Default Company Name';
                $listCompany->address_1 = 'Default Company Address';
                $listCompany->city = 'Default City';
                $listCompany->zip_code = '75008';
            }
            // create company settings for the list
            $listCompany->save();
            $listDefault->list_id = $list->list_id;
            // create default list settings for the list
            $listDefault->save();
            // add customer email to the list
            $subscriber = new ListSubscriber();
            $subscriber->list_id = $list->list_id;
            $subscriber->email = $customer->email;
            $subscriber->source = ListSubscriber::SOURCE_WEB;
            $subscriber->ip_address = $request->getServer('HTTP_X_MW_REMOTE_ADDR', $request->getServer('REMOTE_ADDR'));
            $subscriber->status = ListSubscriber::STATUS_CONFIRMED;
            $blacklisted = $subscriber->getIsBlacklisted(array('checkZone' => EmailBlacklist::CHECK_ZONE_LIST_SUBSCRIBE));
            if (empty($blacklisted)) {
                // if customer email is not blacklisted add email to the created list
                $subscriber->save();
            }
        }
    }

}
